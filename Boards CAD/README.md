# File-Folder Description
The printed circuit assemblies, aka "Cards" of the PolyVent Control Module

|Card Name  | Card Function | Card Schematic | Card PCB|
| ---| --- | ---| --- |
|backplane | Schematic and PCB for a back plane to the PolyVent |  |  |
|bland_card | A schematic and PCB forming a template for future card development. Has PCB outline and connector with signal names | | |
 |esp32_main_card. | A schematic and PCB for the main micro controler for the PolyVent. The ESP32 is on a development "ESP32-DEVKITC-32D" | | |
| motor_control_card | A schematic and PCB for the Sparkfun SAM_D21_MINI_BREAKOUT.| | |
| prototype_card/board  | A schematic and PCB for protopyping new functions on PCB cards for the PolyVent.| | |
| sensor_card  | A schematic and PCB with four pressure sensors, one logic level translator for flow sensor and Op Amp for O2 sensor and Analog to Digital Converter.| | |
| valves_card |A schematic and PCB which is comprized of a JP1_SPARKFUN-SAMD21-MINI-BREAKOUT-V10, optical isolation to the gates of FETs to sink current for valves. A screw terminal strip is provide for valve connections. | | |




