The PolyVent Sensing card can have up to four HoneyWell TruStability pressure sensors slotted into it.

How to select pressure sensors:
The sensing card can only interface with 3.3V SPI pressure sensors. They also need to have a DIP-8 package. 
Simply put, if the part number of your honeywell HSC pressure sensor starts with HSCD or SCCD and ends with SA3, it will interface electrically.

The sensors need to fit into the card and rack, so selecting sensors with small horizontal barbs is ideal. 

You'll also need to select sensors specific to your application. 
On a PolyVent machine you'll need a sensor that can measure breathing pressures accurately (less than 1psi), and a tank sensor that can read to at least 50psi. 

These are good recommendations for polyvent pressure sensors:

Breathing pressure sensor: SCCDRRV100MDSA3

Tank pressure sensor: HSCDRNN100PGSA3


Making the sensors swappable and removable:
The ideal way to mount these sensors is by using sockets. DIP8 chip sockets are readily available, but they will have to be cut in half to fit the 
pressure sensors. They can also be soldered onto the board. Surface mount honeywell sensors can be soldered onto the sensor card, but they cannot be used with sockets. 

For refrerence, here is the honeywell HSC datasheet: https://www.mouser.com/datasheet/2/187/honeywell-sensing-trustability-hsc-series-high-acc-708740.pdf