
The PolyVent backplane has three power rails, one to drive high power devices, and two logic level lines of 5V and 3.3V. 

### 2.1.1 Maximum current draws

Any single card cannot draw more than this much power through each of its lines (because of how much current the backplane connectors can take):
| power rail | line voltage | max current draw (A) |
| ------ | ------ | ------ |
| POW | 12V-24V | 12A |
| 3.3V | 3.3V | 4A |
| 5V | 5V | 4A |

The whole system cannot draw more than this much power (because of how much power the backplane traces + power plug can take). These numbers assume a trace temperature rise of 30°C.
| power rail | backplane V1.1 | backplane V1.2 1oz Cu | backplane V1.2 2oz Cu |
| ------ | ------ |------ |------ |
| POW |12A |16A|20A|
| 3.3V |6.5A | 10.5A |17.5A|
| 5V |6.5A| 10.5A |17.5A|
|Total draw|16A|25A|40A|  
     
     
     
### 2.1.2 Backplane power supplies
The official polyvent control module uses this power system:
![Screen_Shot_2021-10-21_at_6.01.10_PM](uploads/7030ee3eef764cd49d5c4b33e7d0ba67/Screen_Shot_2021-10-21_at_6.01.10_PM.png)

Three power supplies are used so that the reliability of each rail is greater, and that there aren't different power supplies dependent on each other. 

### 2.1.3 Logic level voltage specification
The logic level voltage of the control module is 3.3V, the backplane SPI, I2C and Reset lines are all 3.3V logic. The only exception, however, is the six built-in USB logic lines that run on 5V logic. Any custom card that runs on 1.8V or 5V logic internally must have level shifters built into it so that it can interface with other 3.3V cards in the system. This allows it to interface with existing cards. The level shifters must be able to run at high frequencies. 

### 2.1.4 Clock frequencies for SPI and I2C
TBD

### 2.1.5 I2C addresses taken up by the system (officially recognized only)
TBD

### 2.1.6 DIN41612 connector pinout 
All cards plugged into the system must follow this exact pinout for the din41612 48 pin backplane connector:
![connector](uploads/45ae48e67b349ff052a136c0026b391d/connector.png)
also found in [issue 49](https://gitlab.com/polyvent/polyvent_control_module/-/issues/49)
Notes: 
- Pins 1-16 map to the first row, 17-32 to the second and so on. 
- Pin 1 in in the upper left corner of the backplane when the writing is right side up

### 2.1.7 Card order
For maximum power efficiency, the more power-hungry cards should be further to the right of the board than the lower power cards. The right of the board is the side with the screw terminals and serial number.

### 2.1.8 Card CS channel defaults (Official cards only)
| Card | CS channels used |
| ------ | ------ |
|[ PolyVent stepper motor control card](3.1-PolyVent-Stepper-Motor-Control-Card)| 1, 2, 3 |
| [ PolyVent solenoid valve control card](3.4-PolyVent-Valve-Card) | 4 |
| [ PolyVent sensing card](3.2-PolyVent-Sensing-Card) | 5, 6, 7, 8, 9 |
| [ PolyVent ESP32 microcontroller card](3.3-PolyVent-Microcontroller-ESP32-Card) | none, it drives the whole system |
| [ PolyVent prototype card](3.5-PolyVent-Prototype-Card) | depends on prototyper selection|
   


![Screen_Shot_2021-10-22_at_10.30.47_PM](uploads/f48c670ec8fa9d454bdbd27266d6a19f/Screen_Shot_2021-10-22_at_10.30.47_PM.png)
[sheet link](https://docs.google.com/spreadsheets/d/1GymJlgsOzunp2d3-QfArZbLObRxUg_Ej7q3RqDPNk68/edit?usp=sharing)
