### 4.2.1 Schematic of the proportional valve ventilator (type 1)
![Screen_Shot_2021-11-15_at_5.45.14_PM](uploads/cf02e31b40d87639882fe4e81a622d09/Screen_Shot_2021-11-15_at_5.45.14_PM.png)  
[pdf version of this](uploads/a79742730f7990063d6d00e663d0f4fe/proportional_configuration.pdf)   
There is also an eagle schematic of this in the repo.

For reference, this is the pneumatic schematic of the proportional ventilator
![PolyVent_Proportional_Ventilator_flow_diagram](uploads/d9ef16e89292066f4733dd4e96e209b7/PolyVent_Proportional_Ventilator_flow_diagram.png)

