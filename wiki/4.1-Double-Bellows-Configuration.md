### 4.1.1 Schematic of the configuration:
![Screen_Shot_2021-10-22_at_11.04.10_AM](uploads/24a4081eaf6eababa5ea125bebd2c2a7/Screen_Shot_2021-10-22_at_11.04.10_AM.png)
[pdf version of this](uploads/7901b9520f0ce853018758ee2997a89d/double_bellows_configuration.pdf)   
There's an eagle schematic file in the repo for this too.