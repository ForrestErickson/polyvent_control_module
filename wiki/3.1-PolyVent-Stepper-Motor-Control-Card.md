image of the card when built
# !!! Important: The Current implementation of this card doesn't work properly yet !!!
### 3.1.1 Purpose of the card
The polyvent stepper motor control card is designed to control up to two NEMA 17 or 23 stepper motors, and their endstops. It does this in a way that doesn't take many CPU resources, as stepper motors and limits switch interrupts normally do. 
### 3.1.2 Card features
- SPI interface, with SPI configurable drive settings
- DRV8711 stepper drivers. [Datasheet](https://www.ti.com/lit/ds/symlink/drv8711.pdf?ts=1634887035337&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FDRV8711)
- Controls two bipolar stepper motors
- Endstop sensing built-in
- Acceleration, velocity and position control of the steppers (Higher order derivatives can be programmed in software)
- 48 MHz SAMD21 microcontroller
- up to 1/256 microstepping
- maximum step rate of x Hz (TBD)

### 3.1.3 Electrical specifications
follows all polyvent control module standards

Default SPI CS pins for each device on the board:
| Device | default SPI CS pin |
| ------ | ------ |
|SAMD21 microcontroller (for position, velocity and acceleration control)| CS_1 |
| Motor Driver 1 (to program motor driving settings)| CS_2 |
| Motor Driver 2 (to program motor driving settings)| CS_3 |


### 3.1.4 Front plate mechanical specifications
in the CAD, will be added to the repo shortly

